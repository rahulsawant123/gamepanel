var React = require('react');
var ReactDOM = require('react-dom');
import Master from './components/Master'

if (location.protocol != 'https:')
{
 location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}else{

    ReactDOM.render(
        <Master/>,
        document.getElementById('root')
    );
}
